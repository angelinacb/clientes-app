import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Cliente } from './clientes/cliente';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClientesBusca } from './clientes/clientes-lista/clientesBusca';
import { Telefone } from './telefone/telefone';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  url: string = environment.apiBaseUrl + "/api/clientes"

  constructor( private http: HttpClient ) {}

  salvar( cliente: Cliente ) : Observable<Cliente> {
    return this.http.post<Cliente>(this.url, cliente);
  }

  //Salva o telefone e o id do cliente relacionado
  salvarTelefone( telefone: Telefone, cliente: Cliente ) : Observable<Telefone> {
    return this.http.post<Telefone>(`${this.url}/${cliente.id}`, telefone);
  }

  atualizar( cliente: Cliente ) : Observable<any> {
    return this.http.put<Cliente>(`${this.url}/${cliente.id}`, cliente);
  }   

  getClientes() : Observable<ClientesBusca[]> {
    return this.http.get<ClientesBusca[]>(this.url);
  } 

  getClienteById(id:number) : Observable<Cliente> {
    return this.http.get<any>(`${this.url}/${id}`);
  } 

  deletar(cliente: Cliente) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${cliente.id}`);
  }

  //Realiza a pesquisa de Clientes através dos parâmetros nome e/ou ativo
  buscar(nome: string, ativo: number) : Observable<ClientesBusca[]>{

    const httpParams = new HttpParams()
      .set("nome", nome)
      .set("ativo", ativo ? ativo.toString() : '');

    const url = this.url + "?" + httpParams.toString();
    
    return this.http.get<any>(url);
  }
}
