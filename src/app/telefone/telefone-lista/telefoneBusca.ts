import { Cliente } from '../../clientes/cliente';

export class TelefoneBusca { 
    id: number;       
    numero: string; 
    cliente: Cliente;
}