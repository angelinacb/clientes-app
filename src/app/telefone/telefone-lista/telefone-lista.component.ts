import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TelefoneService } from 'src/app/telefone.service';
import { ClientesService } from 'src/app/clientes.service';
import { Telefone } from '../telefone';
import { TelefoneBusca } from './telefoneBusca';
import { ClientesBusca } from 'src/app/clientes/clientes-lista/clientesBusca';

@Component({
  selector: 'app-telefone-lista',
  templateUrl: './telefone-lista.component.html',
  styleUrls: ['./telefone-lista.component.css']
})
export class TelefoneListaComponent implements OnInit {

  mensagemSucesso: string;
  mensagemErro: string;
  telefoneSelecionado: Telefone;
  clientes: ClientesBusca[] = [];
  lista: TelefoneBusca[]= [];

  // Variáveis usadas na pesquisa de telefones
  message: string;
  id_cliente: number;

  constructor(private serviceTelefone : TelefoneService,
              private serviceCliente : ClientesService,
              private router: Router) { }

  ngOnInit(): void {
    
    //Popula o combo de clientes
    this.serviceCliente
      .getClientes()
      .subscribe( 
        resposta => {          
          this.clientes = resposta;          
        }); 
  }

  preparaDelecao(telefone: Telefone){
    this.telefoneSelecionado = telefone;
  }

  deletarTelefone(){
    this.serviceTelefone
      .deletar(this.telefoneSelecionado)
      .subscribe( 
        response => {
          this.mensagemSucesso = 'Telefone deletado com sucesso!'
          this.lista = []; 
        },
        erro => this.mensagemErro = 'Ocorreu um erro ao deletar o telefone.'  
      )
  }

  consultar(){    

    //Se o cliente foi selecionado para consulta, faz a pesquisa dos telefones
    if (this.id_cliente) {
      this.serviceTelefone
      .buscar(this.id_cliente)
      .subscribe(response => {        
        this.lista = response;
        if (this.lista.length <= 0){
          this.message = "Nenhum Registro encontrado.";
        } else{
          this.message = null;
        }
      });
    } else {
        this.message = 'Selecione o cliente para pesquisar os seus telefones.'
    }    
  }
}
