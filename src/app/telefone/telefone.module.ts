import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TelefoneRoutingModule } from './telefone-routing.module';
import { TelefoneFormComponent } from './telefone-form/telefone-form.component';
import { TelefoneListaComponent } from './telefone-lista/telefone-lista.component';

@NgModule({
  declarations: [
    TelefoneFormComponent, 
    TelefoneListaComponent
  ],
  imports: [
    CommonModule,
    TelefoneRoutingModule,
    FormsModule,
    RouterModule
  ], 
  exports: [
    TelefoneFormComponent, 
    TelefoneListaComponent
  ]
})
export class TelefoneModule { }
