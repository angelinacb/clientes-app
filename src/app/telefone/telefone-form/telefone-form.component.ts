import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { Cliente } from '../../clientes/cliente';
import { ClientesService } from '../../clientes.service';
import { Telefone } from '../telefone';

@Component({
  selector: 'app-telefone-form',
  templateUrl: './telefone-form.component.html',
  styleUrls: ['./telefone-form.component.css']
})
export class TelefoneFormComponent implements OnInit {  

  cliente: Cliente;
  telefone: Telefone;
  success: boolean = false;
  errors: String[];
  id_cliente: number;

  constructor(
    private serviceCliente: ClientesService,  
    private router: Router,
    private acitvatedRoute: ActivatedRoute
  ) { 
      this.telefone = new Telefone();
    }

  voltarParaListagem() {
    this.router.navigate(['/clientes-lista'])
  }

  ngOnInit(): void {
    
    let params : Observable<Params> = this.acitvatedRoute.params
    params.subscribe( urlParams => {
      this.id_cliente = urlParams['id'];
      if(this.id_cliente){
        this.serviceCliente
          .getClienteById(this.id_cliente)
          .subscribe( 
            response => this.cliente = response ,
            errorResponse => this.cliente = new Cliente()
            )
          }
        })
      }

  onSubmit() { 

    // Se possui cliente salva o telefone
    if (this.cliente) {    

      this.serviceCliente
        .salvarTelefone(this.telefone, this.cliente)
        .subscribe( response => {                     
          this.success = true;
          this.errors = null;
          this.telefone = new Telefone();              
        }, errorResponse => {        
          this.success = false;        
          this.errors = errorResponse.error.errors;               
        })    
      }
      else {
        this.success = false; 
        this.errors = ['Erro ao salvar o telefone.']
      }
    }

    

}
