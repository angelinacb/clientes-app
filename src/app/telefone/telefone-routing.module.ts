import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TelefoneFormComponent } from './telefone-form/telefone-form.component';
import { TelefoneListaComponent } from './telefone-lista/telefone-lista.component';

const routes: Routes = [
  { path: 'telefone-form', component: TelefoneFormComponent },
  { path: 'telefone-form/:id', component: TelefoneFormComponent },
  { path: 'telefone-lista', component: TelefoneListaComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TelefoneRoutingModule { }
