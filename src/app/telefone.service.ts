import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Telefone } from './telefone/telefone';
import { TelefoneBusca } from './telefone/telefone-lista/telefoneBusca';

@Injectable({
  providedIn: 'root'
})
export class TelefoneService {
  
  url: string = environment.apiBaseUrl + "/api/telefones"
  
  constructor( private http: HttpClient ) {}

  //Deleta o telefone
  deletar(telefone: Telefone) : Observable<any> {
    return this.http.delete<any>(`${this.url}/${telefone.id}`);
  }

  //Realiza a pesquisa de Telefones através do parâmetro id do cliente
  buscar(id_cliente: number) : Observable<TelefoneBusca[]>{

    const httpParams = new HttpParams()     
      .set("id", id_cliente ? id_cliente.toString() : '');

    const url = this.url + "?" + httpParams.toString();
    
    return this.http.get<any>(url);
  }

}
