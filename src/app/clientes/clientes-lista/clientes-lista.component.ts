import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientesService } from 'src/app/clientes.service';
import { Cliente } from '../cliente';
import { ClientesBusca } from './clientesBusca';

@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.css']
})
export class ClientesListaComponent implements OnInit {

  clientes: ClientesBusca[] = [];
  clienteSelecionado: Cliente;
  mensagemSucesso: string;
  mensagemErro: string;

  // Variáveis usadas na pesquisa de clientes
  nome: string;
  ativo: number;
  message: string;

  constructor(private service : ClientesService,
              private router: Router) {}

  ngOnInit(): void {
    this.service
      .getClientes()
      .subscribe( 
        resposta => {
          this.clientes = resposta;          
        });        
  }

  novoCadastro() {
    this.router.navigate(['/clientes-form'])
  }

  preparaDelecao(cliente: Cliente){
    this.clienteSelecionado = cliente;
  }

  deletarCliente(){
    this.service
      .deletar(this.clienteSelecionado)
      .subscribe( 
        response => {
          this.mensagemSucesso = 'Cliente deletado com sucesso!'
          this.ngOnInit();
        },
        erro => this.mensagemErro = 'Ocorreu um erro ao deletar o cliente.'  
      )
  }

  consultar(){    
    if (this.nome || this.ativo) { 
      this.service
        .buscar(this.nome, this.ativo)
        .subscribe(response => {
          this.clientes = response;
          if (this.clientes.length <= 0) {
            this.message = "Nenhum Registro encontrado.";
          } else {
            this.message = null;
          }
        });
    } else {
      this.message = 'Digite pelo menos um campo para realizar a pesquisa.'
    }
  }
}
