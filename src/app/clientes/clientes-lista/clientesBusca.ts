import { Telefone } from '../../telefone/telefone';

export class ClientesBusca {
    id: number;
    nome: string;
    cpf: string;
    dataCadastro: string;
    ativo: number;

    telefones: Telefone[];
}