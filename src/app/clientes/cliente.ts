export class Cliente {
    id: number;
    nome: string;
    tipo: number;
    cpf: string;
    cnpj: string;
    rg: string;
    ie: string;
    dataCadastro: string;
    ativo: number;   
}